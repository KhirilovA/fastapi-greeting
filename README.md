# FastAPI greeting

## Stack
FastAPI

SQLAlchemy

Jinja2

Pydantic

Poetry

## Running
In backend directory

Add your database data to .env file

Create a table
```
python app.create_tables.py
```

Start an app

```
python app.main.py
```



