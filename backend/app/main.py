import os

from fastapi import FastAPI, Depends
from fastapi.openapi.docs import get_swagger_ui_html
from fastapi.openapi.utils import get_openapi
from starlette.responses import JSONResponse
from starlette.requests import Request
from dotenv import load_dotenv
import uvicorn

from api.api_v1.routers.api_router import api_router
from api.api_v1.routers.api_auth_router import auth_router
from core.auth import get_current_active_superuser
from webapp.greeting_router import greeting_router
from db.session import SessionLocal
from db.schemas import User

load_dotenv()

app = FastAPI(
    title=os.getenv("PROJECT_NAME"), docs_url=None, redoc_url=None, openapi_url=None
)


@app.middleware("http")
async def db_session_middleware(request: Request, call_next):
    request.state.db = SessionLocal()
    response = await call_next(request)
    request.state.db.close()
    return response


@app.get("/openapi.json")
async def get_open_api_endpoint(current_user: User = Depends(get_current_active_superuser)):
    return JSONResponse(get_openapi(title="FastAPI", version=1, routes=app.routes))


@app.get("/docs")
async def get_documentation(current_user: User = Depends(get_current_active_superuser)):
    return get_swagger_ui_html(openapi_url="/openapi.json", title="docs")


# Routers
app.include_router(
    api_router,
    prefix="/api/v1",
    tags=["api_greeting"],
)

app.include_router(
    greeting_router,
    prefix="",
    tags=["greeting"],
)

app.include_router(auth_router, prefix="/api", tags=["auth"])
if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", port=8888)
