from fastapi import (
    APIRouter,
    Depends,
    HTTPException,
    status
)
from sqlalchemy.orm import Session
from sqlalchemy.exc import IntegrityError
import typing as t

from db.session import get_db
from db.crud import (
    get_names,
    get_name,
    create_name,
    delete_name
)
from db.schemas import NameBase, Name


api_router = r = APIRouter()


@r.get(
    "/names",
    response_model=t.List[Name],
    response_model_exclude_none=True,
)
async def names_list(
    db: Session = Depends(get_db),
):
    """
    Get all users
    """
    names = get_names(db)
    return names


@r.get(
    "/names/{name_id}",
    response_model=Name,
    response_model_exclude_none=True,
)
async def name_details(
    name_id: int,
    db: Session = Depends(get_db),
):
    """
    Get any user details
    """
    name = get_name(db, name_id)
    return name


@r.post("/names", response_model=NameBase, response_model_exclude_none=True)
async def name_create(
    name: NameBase,
    db: Session = Depends(get_db),
):
    """
    Create a new user
    """
    try:
        return create_name(db, name)
    except IntegrityError:
        raise HTTPException(status.HTTP_404_NOT_FOUND, detail="Name not found")


@r.delete(
    "/names{name_id}", response_model=Name, response_model_exclude_none=True
)
async def name_delete(
    name_id: int,
    db: Session = Depends(get_db),
):
    """
    Delete existing user
    """
    return delete_name(db, name_id)
