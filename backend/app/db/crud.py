from fastapi import HTTPException, status, Response
from sqlalchemy.orm import Session
from sqlalchemy.sql.expression import text
import typing as t

from . import models, schemas
from core.security import get_password_hash


def get_name(db: Session, name_id: int):
    statement = text('''SELECT name.id AS name_id, name.first_name AS name_first_name, name.last_name AS name_last_name 
    FROM name 
    WHERE name.id = :name_id ''')
    name = db.query(models.Name).from_statement(statement).params(name_id=name_id).first()
    if not name:
        raise HTTPException(status_code=404, detail="User not found")
    return name


def get_names(
    db: Session, skip: int = 0, limit: int = 100
) -> t.List[schemas.Name]:
    # return db.query(models.Name).offset(skip).limit(limit).all()
    statement = text('''SELECT name.id AS name_id, name.first_name AS name_first_name,
    name.last_name AS name_last_name
    FROM name
    LIMIT :limit OFFSET :skip''')
    return db.query(models.Name).from_statement(statement).params(limit=limit, skip=skip).all()


def create_name(db: Session, name: schemas.NameBase) -> models.Name:
    db_name = models.Name(
        first_name=name.first_name,
        last_name=name.last_name,
    )
    # db.add(db_name)
    db.execute(text("INSERT INTO name (first_name, last_name) VALUES (:first_name, :last_name)"),
               {"first_name": name.first_name, "last_name": name.last_name})
    db.commit()
    # db.refresh(db_name)
    return db_name


def delete_name(db: Session, name_id: int) -> Response:
    name = get_name(db, name_id)
    if not name:
        raise HTTPException(status.HTTP_404_NOT_FOUND, detail="User not found")
    # db.delete(name)
    db.execute(text("DELETE FROM name WHERE name.id = :name_id"),
               {"name_id": name_id})
    db.commit()
    return Response(status_code=status.HTTP_204_NO_CONTENT)


def create_user(db: Session, user: schemas.UserCreate) -> models.User:
    hashed_password = get_password_hash(user.password)
    db_user = models.User(
        username=user.username,
        email=user.email,
        is_active=user.is_active,
        is_superuser=user.is_superuser,
        hashed_password=hashed_password,
    )
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def get_user_by_email(db: Session, email: str) -> models.User:
    return db.query(models.User).filter(models.User.email == email).first()
