from sqlalchemy import (
    Column,
    Integer,
    String,
    UniqueConstraint,
    Boolean
)
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Name(Base):
    __tablename__ = "name"

    __table_args__ = (
        UniqueConstraint('first_name', 'last_name'),
    )

    id = Column(Integer, primary_key=True, index=True)
    first_name = Column(String)
    last_name = Column(String)


class User(Base):
    __tablename__ = "user"

    id = Column(Integer, primary_key=True, index=True)
    email = Column(String, unique=True, index=True, nullable=False)
    username = Column(String)
    hashed_password = Column(String, nullable=False)
    is_active = Column(Boolean, default=True)
    is_superuser = Column(Boolean, default=False)
