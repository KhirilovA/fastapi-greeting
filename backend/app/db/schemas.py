from pydantic import BaseModel


class NameBase(BaseModel):
    first_name: str
    last_name: str

    class Config:
        orm_mode = True


class Name(NameBase):
    id: int


class UserBase(BaseModel):
    email: str
    is_active: bool = True
    is_superuser: bool = False
    username: str = None

    class Config:
        orm_mode = True


class User(UserBase):
    id: int


class UserCreate(UserBase):
    password: str


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    email: str = None
    permissions: str = "superuser"
