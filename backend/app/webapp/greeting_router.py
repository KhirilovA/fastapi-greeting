import base64
from datetime import timedelta

from fastapi import (
    APIRouter,
    Depends,
    Request,
    HTTPException
)
from fastapi.encoders import jsonable_encoder
from fastapi.templating import Jinja2Templates
from sqlalchemy.orm import Session
from sqlalchemy.exc import IntegrityError
from starlette.responses import RedirectResponse, Response

from db.crud import (
    create_name,
    get_names
)
from db.session import get_db
from db.schemas import NameBase
from core.auth import authenticate_user
from core.security import (
    BasicAuth,
    create_access_token,
    ACCESS_TOKEN_EXPIRE_MINUTES
)
from .form import NameGreetForm


greeting_router = APIRouter(include_in_schema=False)
templates = Jinja2Templates(directory="templates")

basic_auth = BasicAuth(auto_error=False)


@greeting_router.get("/greet/")
def get_greeting(request: Request):
    return templates.TemplateResponse("greeting.html", {"request": request})


@greeting_router.post("/greet/")
async def create_greeting(request: Request, db: Session = Depends(get_db)):
    form = NameGreetForm(request)
    await form.load_data()
    name = NameBase(first_name=form.first_name, last_name=form.last_name)
    try:
        create_name(name=name, db=db)
        form.__dict__["message"] = f"Hi, {form.first_name} {form.last_name}"
        return templates.TemplateResponse("greeting.html", form.__dict__)
    except IntegrityError:
        form.__dict__["errors"] = f"Seen you, {form.first_name}"
        return templates.TemplateResponse("greeting.html", form.__dict__)


@greeting_router.get("/name_list")
def home(request: Request, db: Session = Depends(get_db)):
    names = get_names(db=db)
    return templates.TemplateResponse("name_list.html", {"request": request, "names": names})


@greeting_router.get("/login_basic")
async def login_basic(db: Session = Depends(get_db), auth: BasicAuth = Depends(basic_auth)):
    if not auth:
        response = Response(headers={"WWW-Authenticate": "Basic"}, status_code=401)
        return response

    try:
        decoded = base64.b64decode(auth).decode("ascii")
        username, _, password = decoded.partition(":")
        user = authenticate_user(db, username, password)
        if not user:
            raise HTTPException(status_code=400, detail="Incorrect email or password")

        access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
        access_token = create_access_token(
            data={"sub": username}, expires_delta=access_token_expires
        )

        token = jsonable_encoder(access_token)
        print(token)
        response = RedirectResponse(url="/docs")
        response.set_cookie(
            "Authorization",
            value=f"Bearer {token}",
            httponly=True,
            max_age=1800,
            expires=1800,
        )
        return response

    except:
        response = Response(headers={"WWW-Authenticate": "Basic"}, status_code=401)
        return response
