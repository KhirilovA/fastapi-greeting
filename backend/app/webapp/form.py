from typing import Optional
from fastapi import Request


class NameGreetForm:
    def __init__(self, request: Request):
        self.request: Request = request
        self.first_name: Optional[str] = None
        self.last_name: Optional[str] = None

    async def load_data(self):
        form = await self.request.form()
        self.first_name = form.get("first_name")
        self.last_name = form.get("last_name")
